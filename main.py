from pymongo import MongoClient
client = MongoClient('localhost', 27017) 
mydatabase = client['PPAP_OEE'] 
# print(mydatabase)
# print(mycollection)

# 1. GET ALL PRODUCTION ORDERS
"""
mycollection = mydatabase['production_orders'] 
cursor = mycollection.find() 
for i in cursor:
    print(i) 
"""

# 2. GET PRODUCTION ORDERS AFTER 24 September
"""
mycollection = mydatabase['temp_production_orders'] 
cursor = mycollection.find(
    { "DATE": {"$gt": "24-Sep-2020"}   }
) 
for i in cursor:
    print(i) 
"""

# 3. Get SHIFT RECORDS WITH "SHIFT START DATE" as 23 September or 24 Sept
"""
mycollection = mydatabase['shift_records'] 
cursor = mycollection.find(
    { "SHIFT DATE": {"$in": ["23/09/2020", "24/09/2020"]}   }
) 
for i in cursor:
    print(i) 
"""